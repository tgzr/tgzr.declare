# This will register all component renderers:
from . import (
    containers,
    widget,
    elements,
    controls,
    item_view,
    others,
)
