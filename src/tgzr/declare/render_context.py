from functools import partial
from contextlib import contextmanager


class Bindable(object):
    """
    A Bindable is a value that can be get, set or binded to and state in the store state.
    They are managed by RenderContext and should not be manipulated by anything else.
    """

    def __init__(self, name, getter, setter):
        """
        Create a Bindable with this name. The getter and setter
        callback must have signatures:
            `getter(context)`
            `setter(context, value)`

        """
        super().__init__()
        self._name = name
        self._getter = getter
        self._setter = setter

        self._binded_state_key = None
        self._binded_updater = None

    def __repr__(self):
        return f"Bindable[{self._name}]"

    def name(self):
        return self._name

    def get(self, context):
        return self._getter(context)

    def set(self, context, value):
        self._setter(context, value)

    def bind(self, context, state_key):
        if self._binded_state_key is not None:
            print(
                RuntimeError(
                    f"Property '{self._name}' is already binded to state {state_key!r}, cannot also bind to {state_key}."
                )
            )
        self._binded_state_key = state_key
        updater = context.bind_state(state_key, partial(self.set, context))
        self._binded_updater = updater

    def update(self, context):
        if self._binded_updater is not None:
            self._binded_updater()
        else:
            # This may not seam obvious at first, but even not-binded property
            # need to trigger some update (when a property affects another one,
            # see the `format` property of the Text component in Qt fro example...)
            self.set(context, self.get(context))


class RenderContext(object):
    @classmethod
    def create_root_context(cls, renderer, **overrides):
        overrides["renderer"] = renderer
        return cls(None, overrides)

    def __init__(self, parent_context, overrides):
        self._parent_context = parent_context
        self._child_contexts = []

        self._state_bindings = []
        self._on_clear_callbacks = []

        self._overrides = {}
        # use __setitem__ so that we only store
        # new values:
        for name, value in overrides.items():
            self[name] = value

    #
    #   PROPERTIES
    #
    def register_property(self, property_name, getter, setter):
        """
        Registers the setter and the getter for a property with the given name.
        Their signature must be like:
            `getter(context)`
            `setter(context, value)`
        The context parameter will receive the context in which the property was
        registered.

        Raises a ValueError if such a property has already been registered
        in this context. (Note that properties declared in parent context(s) are
        overridable, only the current context forbids registering on an existing
        property name.)

        """
        try:
            properties = self._overrides["__properties__"]
        except KeyError:
            properties = {}
            self._overrides["__properties__"] = properties

        if property_name in properties:
            raise ValueError("Property override is not allowed !!!")

        property = Bindable(property_name, getter, setter)
        properties[property_name] = property

    def _find_property(self, property_name):
        """
        Returns the getter and the setter for the given property name.

        The lookup is delegated to the parent context if this no such
        property was registered in this context.

        Raise KeyError if no property with this name has been registered
        in this context and the parent contexts.
        """
        try:
            # Wow ! that actually the first time in my life I have
            # two lines inside a try, and it make complete sens !!! \o/
            # Achievment unlocked ^^
            # (Hint: both lines would raise a KeyError, so I can merge the two `try` statements)
            properties = self._overrides["__properties__"]
            return properties[property_name]
        except KeyError:
            if self._parent_context is None:
                raise KeyError(f'Could not find property "{property_name}".')
            return self._parent_context._find_property(property_name)

    def get_property(self, property_name):
        """
        Returns the value of this property.
        """
        property = self._find_property(property_name)
        return property.get(self)

    def set_property(self, property_name, value):
        """
        Change the value of this property.
        """
        property = self._find_property(property_name)
        property.set(self, value)

    def update_property(self, property_name):
        property = self._find_property(property_name=property_name)
        property.update(self)

    def bind_property(self, property_name, state_key):
        """
        Sync the property value with the given state value.
        """
        property = self._find_property(property_name)
        property.bind(self, state_key)

    #
    #   STATES
    #

    def set_state(self, name, value):
        self["renderer"].update_states({name: value})

    def get_state(self, name, *default):
        return self["renderer"].get_state(name, *default)

    def get_states(self, prefix=None, strip_prefix=True):
        return self["renderer"].get_states(prefix, strip_prefix)

    def bind_state(self, state_key, update_callback, *default):
        """
        Calls the given callback when the given state changes.
        Returns a callback to use to trigger a call to update_callback
        with the current value.
        """
        self._state_bindings.append((state_key, update_callback))
        return self["renderer"].bind_state(state_key, update_callback, *default)

    def unbind_state(self, state_key, update_callback):
        """
        Unbind a state. The `update_callback` must have been use with the same
        `state_key` on a previous call to `bind_state()`.
        """
        self["renderer"].unbind_state(state_key, update_callback)

    def clear_state_bindings(self):
        for state_key, update_callback in self._state_bindings:
            self.unbind_state(state_key, update_callback)
        self._bindings = []

    #
    #   ACTIONS
    #
    def get_handler(self, key, action="action"):
        """
        Returns a callable without argument that will
        perform the action.
        """
        renderer = self["renderer"]
        return partial(renderer.perform_action, key, action, self)

    #
    #   RENDER
    #
    def render_children(self, children, **context_overrides):
        renderer = self["renderer"]
        with self(INFO="children_context", **context_overrides) as context:
            for child in children:
                renderer.render(child, context)

    def render(self, ui):
        return self["renderer"].render(ui, self)

    #
    #   DATA
    #

    @contextmanager
    def __call__(self, **overrides):
        child_context = RenderContext(self, overrides)
        self._child_contexts.append(child_context)
        yield child_context

    def __getitem__(self, name):
        try:
            return self._overrides[name]
        except KeyError:
            if self._parent_context is None:
                raise
            return self._parent_context[name]

    def __setitem__(self, name, value):
        """
        Overrides the value in this context.

        If the given value equals the value inherited from
        parent context, the override is simply removed from
        this context.
        """
        try:
            parent_value = self._parent_context[name]
        except (TypeError, KeyError):
            # parent is None, or parent doesn't have this key
            pass
        else:
            if parent_value == value:
                try:
                    del self._overrides[name]
                except KeyError:
                    pass
                return
        self._overrides[name] = value

    def get(self, name, *default):
        try:
            return self[name]
        except KeyError:
            if default:
                return default[0]
            return None

    #
    #   TREE
    #

    def root_context(self):
        if self._parent_context is None:
            return self
        return self._parent_context.root_context()

    def walk(self, down_callable, up_callable=None):
        stop = down_callable(self)
        if stop:
            return stop
        for context in self._child_contexts:
            stop = context.walk(down_callable)
            if stop:
                return stop
        if up_callable is not None:
            stop = up_callable(self)
            return stop
        return

    def add_on_clear(self, callback):
        self._on_clear_callbacks.append(callback)

    def clear(self):
        for on_clear_callback in self._on_clear_callbacks:
            on_clear_callback(self)
        self._on_clears_callbacks = []
        self.clear_state_bindings()

    def clear_branch(self):
        """
        Calls `context.clear()` on this context and all child contexts.
        """

        def clearer(context):
            context.clear()

        self.walk(clearer)

    #
    #   TOOLS
    #

    def pprint(self, indent=0, overridden=None):
        """
        Prints the values in the context and parent context(s),
        with an arrow pointing a the final value (the ones
        returned when accessed thru this context).
        """
        if overridden is None:
            overridden = set()
        indent_str = "    "
        space = indent * indent_str
        print(f"{space}=== Context ===")
        for name, value in sorted(self._overrides.items()):
            r = repr(value)[:40]
            if name == "__properties__":
                r = "{" + ", ".join([repr(i) + ":..." for i in value.keys()]) + "}"
            f = "   "
            if name not in overridden:
                f = "-> "
                overridden.add(name)
            print(f"{space}{f}{name} = {r}")
        if self._parent_context is not None:
            self._parent_context.pprint(indent + 1, overridden)
